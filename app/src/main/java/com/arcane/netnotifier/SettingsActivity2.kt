package com.arcane.netnotifier

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import kotlin.concurrent.thread

class SettingsActivity2 : AppCompatActivity() {

    private inline val tp
        get() = TypedPreferences(this)
    private val services = SystemServiceUtils(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.netn_settings_activity_2)

        // Apply preference defaults.
        // This should be called by the application's main activity.
        PreferenceManager.setDefaultValues(this, R.xml.pref_monitor, false)

        sendBroadcast(Intent(this, RefreshMonitorReceiver::class.java))
        registerNetworkAvailabilityCallback()

        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.container, MonitorSettingsFragment())
                    .commit()
        }

        // Listen for preference updates.
        LocalBroadcastManager.getInstance(this).registerReceiver(
                object : BroadcastReceiver() {
                    override fun onReceive(context: Context, intent: Intent) {
                        supportFragmentManager
                                .beginTransaction()
                                .replace(R.id.container, MonitorSettingsFragment())
                                .commit()
                    }
                },
                IntentFilter(EVENT_PREFERENCE_UPDATE)
        )
    }

    override fun onStart() {
        // Force send notification.
        tp.connected = TypedPreferences.CONNECTIVITY_UNKNOWN
        super.onStart()
    }

    override fun onResume() {
        thread {
            if (tp.monitorEnabled) {
                // Send notification once.
                NotificationSender(this@SettingsActivity2)
                        .sendConnectivityNotification()
            }

            runBlocking {
                launch(Dispatchers.Main) {
                    // Schedule monitoring.
                    NetworkMonitorJobService.Controller(
                            this@SettingsActivity2).refresh()
                }
            }
        }
        super.onResume()
    }

    private fun registerNetworkAvailabilityCallback() {
        // Register network callback

        val request = NetworkRequest.Builder()
                .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                .build()!!
        val intent = Intent(this, RefreshMonitorReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        )!!

        services.connectivityManager
                .registerNetworkCallback(request, pendingIntent)
    }

    companion object {
        const val EVENT_PREFERENCE_UPDATE = "preference_update"
    }
}
