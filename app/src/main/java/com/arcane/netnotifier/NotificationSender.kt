package com.arcane.netnotifier

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import android.util.Log

class NotificationSender(var context: Context) {

    private val services = SystemServiceUtils(context)

    private inline val nm
        get() = services.notificationManager

    /**
     * Long-running operation
     */
    fun sendConnectivityNotification() {
        sendNotification(
                InternetConnectivityChecker.checkInternetAvailability())
    }

    /**
     * Long-running operation
     */
    fun sendNotification(connected: Boolean) {
        val pref = TypedPreferences(context)

        val previousConnected = when (pref.connected) {
            TypedPreferences.CONNECTIVITY_DISCONNECTED -> false
            TypedPreferences.CONNECTIVITY_CONNECTED -> true
            else -> null
        }

        Log.i(TAG, "previousConnected: $previousConnected; connected: $connected")

        val sameAsPrevious = previousConnected == connected
        val debugModeEnabled = pref.debugModeEnabled
        if (sameAsPrevious && !debugModeEnabled) return

        val updatePrevious = sameAsPrevious && debugModeEnabled

        val getString = { id: Int -> context.resources.getString(id) }

        val notification = createBlankNotificationBuilder()
                .apply {
                    setContentTitle(
                            if (connected) getString(R.string.notif_connect_text)
                            else getString(R.string.notif_disconnect_text))
                    setSmallIcon(
                            if (connected) R.drawable.ic_cloud_queue_white_24dp
                            else R.drawable.ic_cloud_off_white_24dp)
                    if (!updatePrevious) {
                        setSound(RingtoneUtils(context).getRingtoneUri(
                                if (connected) "pref_ringtone_connect"
                                else "pref_ringtone_disconnect"))
                    }
                }
                .build()!!

        nm.notify(NOTIFICATION_ID, notification)

        // Update value of `connected`.
        pref.connected =
                if (connected) TypedPreferences.CONNECTIVITY_CONNECTED
                else TypedPreferences.CONNECTIVITY_DISCONNECTED
    }

    fun cancelNotification() {
        nm.cancel(NOTIFICATION_ID)
    }

    fun createDisablePendingIntent(): PendingIntent {
        val intent = Intent(context, DisableMonitorReceiver::class.java)
        return PendingIntent.getBroadcast(
                context,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT)!!
    }

    fun createBlankNotificationBuilder(): NotificationCompat.Builder {
        return NotificationCompat.Builder(
                context,
                getString(R.string.notif_channel_id))
                .apply {
                    setCategory(Notification.CATEGORY_STATUS)
                    addAction(
                            0,
                            context.resources.getString(
                                    R.string.notif_action_disable),
                            createDisablePendingIntent())
                }
    }

    private inline fun getString(id: Int) = context.resources.getString(id)

    companion object {
        private const val TAG = "NotificationSender"

        private const val NOTIFICATION_ID: Int = R.string.monitor_notification_id
    }

}
