package com.arcane.netnotifier

import android.app.NotificationManager
import android.app.job.JobService
import android.content.Context
import android.net.ConnectivityManager

class SystemServiceUtils(val context: Context) {

    inline val connectivityManager
        get() = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager

    inline val notificationManager
        get() = context.getSystemService(JobService.NOTIFICATION_SERVICE)
                as NotificationManager
}
