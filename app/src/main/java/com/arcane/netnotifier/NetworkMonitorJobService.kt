package com.arcane.netnotifier

import android.app.job.JobInfo
import android.app.job.JobParameters
import android.app.job.JobScheduler
import android.app.job.JobService
import android.content.ComponentName
import android.content.Context
import android.util.Log
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

class NetworkMonitorJobService : JobService() {

    private val cancel = AtomicBoolean(false)
    private val sender = NotificationSender(this)

    override fun onStartJob(params: JobParameters?): Boolean {
        Log.i(TAG, "onStartJob called")

        thread {
            runBlocking {
                runMonitor()
            }
        }

        return true // More work
    }

    private suspend fun runMonitor() {
        while (!cancel.get()) {
            Log.i(TAG, "runMonitor..while")
            sender.sendConnectivityNotification()
            delay(5000)
            Log.i(TAG, "post-delay")
        }
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        Log.i(TAG, "onStopJob called")
        cancel.set(true)
        return true // Reschedule the job if stopped
    }

    companion object {
        private const val TAG = "NetworkMonitorService"
    }

    class Controller(private val context: Context) {

        private inline val scheduler
            get() = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler

        /**
         * Starts the network monitor job service if the setting is enabled and
         * the network is available.
         */
        fun refresh(
                monitorEnabled: Boolean = TypedPreferences(context).monitorEnabled,
                networkAvailable: Boolean = TypedPreferences(context).networkAvailable
        ) {
            if (monitorEnabled && networkAvailable) start() else stop()
        }

        private fun start() {
            val jobInfo = JobInfo.Builder(
                    R.string.monitor_job_id,
                    ComponentName(context, NetworkMonitorJobService::class.qualifiedName!!)
            )
                    .setPersisted(true)
                    .setPeriodic(1000 * 60 * 15)
                    .build()
            scheduler.schedule(jobInfo)
        }

        private fun stop() {
            scheduler.cancel(R.string.monitor_job_id)

            // Cancel existing notification.
            NotificationSender(context).cancelNotification()

            // Reset saved connectivity.
            // This will ensure that the notification is shown again
            // when the service is started.
            TypedPreferences(context).connected =
                    TypedPreferences.CONNECTIVITY_UNKNOWN

            // Send stopped notification.
            if (TypedPreferences(context).debugModeEnabled) {
                val getString = { id: Int -> context.resources.getString(id) }
                val notification = NotificationSender(context)
                        .createBlankNotificationBuilder()
                        .apply {
                            setContentTitle(
                                    getString(R.string.notif_job_end)
                            )
                            setSmallIcon(R.drawable.ic_cloud_off_white_24dp)
                            setSound(
                                    RingtoneUtils(context).getRingtoneUri("pref_ringtone_disconnect")
                            )
                        }
                        .build()!!

                SystemServiceUtils(context).notificationManager.notify(R.string.monitor_notification_id,
                        notification)
            }
        }

    }
}
