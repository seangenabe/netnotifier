package com.arcane.netnotifier

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import kotlin.reflect.KProperty

class TypedPreferences(private val context: Context) {

    private val pref
        get() = PreferenceManager.getDefaultSharedPreferences(context)!!

    private fun edit(fn: SharedPreferences.Editor.() -> Unit) {
        pref.edit().apply {
            fn()
            apply()
        }
    }

    var monitorEnabled: Boolean by SharedPreferencesDelegate(MyType.Boolean, true)
    var networkAvailable: Boolean by SharedPreferencesDelegate(MyType.Boolean, false)
    var connected: Int by SharedPreferencesDelegate(MyType.Int, 0)
    var debugModeEnabled: Boolean by SharedPreferencesDelegate(MyType.Boolean, false)

    companion object {

        const val CONNECTIVITY_UNKNOWN = 0x0
        const val CONNECTIVITY_DISCONNECTED = 0x2
        const val CONNECTIVITY_CONNECTED = 0x4

    }

    private class SharedPreferencesDelegate<T>(
            private val type: MyType,
            private val defaultValue: T,
            private val key: String? = null) {

        @Suppress("UNCHECKED_CAST")
        operator fun getValue(thisRef: TypedPreferences, property: KProperty<*>): T {
            val key: String by lazy { this.key ?: property.name.camelToUnderscore() }
            return thisRef.pref.run {
                when (type) {
                    MyType.Boolean -> getBoolean(key, defaultValue as Boolean) as T
                    MyType.Int -> getInt(key, defaultValue as Int) as T
                }
            }
        }

        operator fun setValue(thisRef: TypedPreferences, property: KProperty<*>, value: T) {
            val key: String by lazy { this.key ?: property.name.camelToUnderscore() }
            thisRef.edit {
                when (type) {
                    MyType.Boolean -> putBoolean(key, value as Boolean)
                    MyType.Int -> putInt(key, value as Int)
                }
            }
        }

    }
}


private enum class MyType {
    Boolean,
    Int
}

private fun String.camelToUnderscore(): String {
    return mapIndexed { index, char ->
        when {
            index == 0 -> "$char"
            char.isUpperCase() -> "_${char.toLowerCase()}"
            else -> "$char"
        }
    }.joinToString("")
}