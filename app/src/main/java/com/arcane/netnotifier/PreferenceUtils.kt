package com.arcane.netnotifier

import android.content.Intent
import android.media.RingtoneManager
import android.preference.ListPreference
import android.preference.PreferenceManager
import android.provider.Settings.System.DEFAULT_NOTIFICATION_URI
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat

fun Preference.isRingtone() = key.startsWith("pref_ringtone_")

fun PreferenceFragmentCompat.ringtoneClick(preference: Preference, requestCode: Int) {
    if (!preference.isRingtone()) return
    val ru = RingtoneUtils(preference.context)
    val intent = Intent(RingtoneManager.ACTION_RINGTONE_PICKER)
            .apply {
                putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION)
                putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
                putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true)
                putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, DEFAULT_NOTIFICATION_URI!!)
                ru.getRingtoneUri(preference.key)?.let {
                    putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, it)
                }
            }

    startActivityForResult(intent, requestCode)
}

fun Preference.bindSummaryToValue() {
    // Set the listener to watch for value changes.
    onPreferenceChangeListener = sBindPreferenceSummaryToValueListener

    // Trigger the listener immediately with the preference's
    // current value.
    updateSummary()
}

fun Preference.updateSummary() {
    sBindPreferenceSummaryToValueListener.onPreferenceChange(
            this,
            PreferenceManager
                    .getDefaultSharedPreferences(context)
                    .getString(key, "")
    )
}

private val sBindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, value ->
    val stringValue = value.toString()

    when {
        preference is ListPreference -> {
            // For list preferences, look up the correct display value
            // in the preference's 'entries' list.
            val index = preference.findIndexOfValue(stringValue)

            // Set the summary to reflect the new value.
            preference.setSummary(
                    if (index >= 0)
                        preference.entries[index]
                    else
                        null
            )
        }
        preference.isRingtone() ->
            // For ringtone preferences, look up the correct display value
            // using RingtoneManager.
            preference.summary = RingtoneUtils(preference.context)
                    .getRingtoneTitle(preference.key)
        else -> // For all other preferences, set the summary to the value's
            // simple string representation.
            preference.summary = stringValue
    }
    true
}
