package com.arcane.netnotifier

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager

/**
 * A broadcast receiver that disables monitoring.
 */
class DisableMonitorReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        TypedPreferences(context).monitorEnabled = false
        NetworkMonitorJobService.Controller(context)
                .refresh(monitorEnabled = false)

        // Broadcast preference update
        LocalBroadcastManager.getInstance(context).sendBroadcast(
                Intent(SettingsActivity2.EVENT_PREFERENCE_UPDATE)
        )
    }

}
