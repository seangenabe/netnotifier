package com.arcane.netnotifier

import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat

class MonitorSettingsFragment : PreferenceFragmentCompat() {

    private var clickedPreference: Preference? = null

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_monitor, rootKey)

        val prefKeys = listOf(
                "pref_ringtone_connect",
                "pref_ringtone_disconnect"
        )
        for (key in prefKeys) {
            findPreference(key).bindSummaryToValue()
        }
        findPreference("monitor_enabled").onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { pref, value ->

                    NetworkMonitorJobService.Controller(pref.context).refresh(
                            monitorEnabled = value as Boolean)

                    // Save changes to the preference.
                    true
                }
    }

    override fun onPreferenceTreeClick(preference: Preference): Boolean {
        return if (preference.isRingtone()) {
            clickedPreference = preference
            ringtoneClick(preference, REQUEST_CODE_RINGTONE)
            true
        } else {
            super.onPreferenceTreeClick(preference)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_RINGTONE && data != null) {
            val ringtoneUri: Uri? = data.getParcelableExtra(
                    RingtoneManager.EXTRA_RINGTONE_PICKED_URI
            )
            clickedPreference?.apply {
                RingtoneUtils(context!!).setRingtoneFromActivityResult(
                        key,
                        ringtoneUri
                )
                updateSummary()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    companion object {
        private const val REQUEST_CODE_RINGTONE = 1
    }
}
