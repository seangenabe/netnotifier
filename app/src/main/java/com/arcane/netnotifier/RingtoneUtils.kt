package com.arcane.netnotifier

import android.content.Context
import android.media.RingtoneManager
import android.net.Uri
import android.preference.PreferenceManager
import android.provider.Settings

// Default sound: Settings.System.DEFAULT_NOTIFICATION_URI
// Silent: "silent"

class RingtoneUtils(
        private val context: Context,
        private val defaultValue: Uri = Settings.System.DEFAULT_NOTIFICATION_URI!!
) {

    private val pref = PreferenceManager.getDefaultSharedPreferences(context)

    fun getRingtoneUri(key: String): Uri? {
        return pref.getString(key, null).let {
            when (it) {
                null -> defaultValue
                "" -> null
                else -> Uri.parse(it)
            }
        }
    }

    fun getRingtoneTitle(key: String): String? {
        return getRingtoneUri(key)?.let {
            val ringtone = RingtoneManager.getRingtone(context, it)
            ringtone?.getTitle(context)
        } ?: context.resources.getString(R.string.pref_ringtone_silent)
    }

    fun setRingtoneFromActivityResult(key: String, ringtoneUri: Uri?) {
        pref.edit().apply {
            putString(key, ringtoneUri?.toString() ?: "")
            apply()
        }
    }

}
