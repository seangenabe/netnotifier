package com.arcane.netnotifier

import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

private const val TIMEOUT = 5000

object InternetConnectivityChecker {

    fun checkInternetAvailability() = try {
        val socket = Socket()
        val socketAddress = InetSocketAddress("8.8.8.8", 53)

        socket.connect(socketAddress, TIMEOUT)
        socket.close()

        true
    } catch (ex: IOException) {
        false
    }

}
