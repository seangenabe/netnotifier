package com.arcane.netnotifier

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

/**
 * A broadcast receiver that refreshes the network monitor.
 */
class RefreshMonitorReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val pref = TypedPreferences(context)
        val services = SystemServiceUtils(context)

        val info = services.connectivityManager.activeNetworkInfo
        val connected = info?.isConnected ?: false
        pref.networkAvailable = connected
        NetworkMonitorJobService.Controller(context)
                .refresh(networkAvailable = connected)
    }

}
